<?php
/*
Title: Document 
Post Type: document-manager-npa
Context: normal
Priority: high
Order: 1
*/

piklist('field', array(
	'type' => 'file'
	,'basic' => true
	,'field' => 'npa_dm_file'
	,'scope' => 'post_meta'
	,'label' => __('Document','document-manager-npa')
	,'options' => array(
		'basic' => true,
		'button' => 'Add File',
	)
));

piklist('field', array(
	'type' => 'datepicker'
	,'scope' => 'post_meta' // Not used for settings sections
	,'field' => 'npa_dm_doc_date'
	,'label' => 'Document Date'
	,'attributes' => array(
		'class' => 'text'
	)
	,'options' => array(
		'dateFormat' => 'M d, yy'
		,'firstDay' => '0'
	)
));