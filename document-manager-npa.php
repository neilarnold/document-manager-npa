<?php
/*
Plugin Name: Document Manager by Neil
Plugin URI: http://www.neilarnold.com
Description: Simple Document Manager with Categories
Version: 0.9
Author: Neil Arnold
Author URI: http://www.neilarnold.com
Plugin Type: Piklist
License: GPL2
*/


// Check for Piklist --- --- ---
function checkForPiklist() {
    if(is_admin())   {
        include_once('includes/class-piklist-checker.php');
        if (!piklist_checker::check(__FILE__))
            return;
    }
}
add_action('init', 'checkForPiklist');
// END - Check for Piklist --- --- ---


// Register Custom Post Type --- --- ---
function npa_dm_init_cpt($post_types)
{
	$post_types['document-manager-npa'] = array(
		'labels' => piklist('post_type_labels', 'Documents')
		,'title' => __('Enter a new Document Title')
		,'public' => true
        ,'rewrite' => array( 'slug' => 'documents' )
		,'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'comments' )
        ,'hide_meta_box' => array( 'slug' , 'author' , 'comments' , 'commentstatus', 'mymetabox_revslider_0' )
        ,'menu_icon' => 'dashicons-media-document'
        ,'has_archive' => true
	);

	return $post_types;
}
add_filter('piklist_post_types', 'npa_dm_init_cpt');

function npa_dm_taxonomy($taxonomies)
{
	$taxonomies[] = array(
		'post_type' => 'document-manager-npa'
		,'name' => 'document-categories-npa'
		,'show_admin_column' => true
		,'configuration' => array(
			'hierarchical' => false
			,'labels' => piklist('taxonomy_labels', 'Category')
			,'hide_meta_box' => false
			,'show_ui' => true
			,'query_var' => true
			,'rewrite' => array(
				'slug' => 'dm-category'
			)
		)
	);
	return $taxonomies;
}
add_filter('piklist_taxonomies', 'npa_dm_taxonomy');
// END - Register Custom Post Type --- --- ---


// Add Shortcode
function npa_dm_shortcode( $atts ) {
	global $npa_dm_enqueue;
	$npa_dm_enqueue = true;
	$html = '<div class="document-manager">';

	$html .= '<div class="document-manager-search">';
	$html .= '<label>Search for Documents:</label>';
	$html .= '<input type="text" name="keywords" id="document-search" placeholder="enter keywords to search through documents" />';
	$html .= '</div>';

	// Attributes
	extract( shortcode_atts(
		array(
			'sort_by' => 'title',
		), $atts )
	);

	$doc_groups = get_terms( 'document-categories-npa' );
	foreach ( $doc_groups as $doc_group ) {
		$doc_qry = new WP_Query( array(
			'post_type' => 'document-manager-npa',
			'order' => 'ASC',
			'orderby' => 'title',
			'nopaging' => true,
			'tax_query' => array(
				array(
					'taxonomy' => 'document-categories-npa',
					'field' => 'slug',
					'terms' => array( $doc_group->slug ),
					'operator' => 'IN'
				)
			)
		) );

		$html .= "<h2 class='dm-section-title'>$doc_group->name ($doc_qry->found_posts)</h2>";
		$html .= "<div class='dm-table-container'>";
		$html .= "<table class='document-manager-table footable' data-sort='false' data-filter='false'>";
		$html .= "<thead><tr>";
    $html .= "<th data-hide='phone, tablet'></th>";
		$html .= "<th>File</th>";
		$html .= "<th>Size</th>";
		$html .= "<th data-hide='phone'>Date</th>";
		$html .= "<th data-hide='phone,tablet'>Download</th>";
		$html .= "<th data-hide='phone'></th>";
		$html .= "</tr></thead>";
		$html .= "<tbody>";

		if ( $doc_qry->have_posts() ) {
			while ( $doc_qry->have_posts() ) {
				$doc_qry->the_post();

				$file = get_post_meta(get_the_ID(), 'npa_dm_file', false);
				$file_path = get_attached_file($file[0]);
				//$title_link = wp_get_attachment_link($file[0], '', false, false, get_the_title());
				//$download_link = wp_get_attachment_link($file[0], '', false, false, 'Download');
				$title_link = wp_get_attachment_url($file[0]);
				$download_link = wp_get_attachment_url($file[0]);
				$icon_img = mime_type_icon($file_path);
				$file_size = get_file_size($file_path);

				$html .= "<tr>";
				$html .= "<td width='33px'>" . $icon_img . "</td>";
				$html .= "<td class='search-this'><a href='" . $title_link . "' target='_blank'>" . get_the_title() . "</a></td>";
				$html .= "<td width='100px'>" . $file_size . "</td>";
				$html .= "<td class='search-this' width='100px'>" . get_post_meta(get_the_ID(), 'npa_dm_doc_date', true) . "</td>";
				$html .= "<td width='100px'><a href='" . $download_link . "' target='_blank'>Download</a></td>";
				$html .= "<td class='search-this desc'>" . get_the_content() . "</td>";
				$html .= "</tr>";
			}
		}
		$html .= "</tbody>";
		$html .= "</table>";
		$html .= "</div>";
		wp_reset_postdata();
	}

	$html .= "</div>";
	return $html;
}
add_shortcode( 'document_manager', 'npa_dm_shortcode' );

function npa_dm_register_scripts() {
	wp_register_script('npa-dm-scripts', plugin_dir_url( __FILE__ ) . 'includes/front-end.js', array('jquery'), '1.0', true);
	wp_register_style('npa-dm-styles', plugin_dir_url( __FILE__ ) .  'includes/front-end.css');
}
function npa_dm_print_scripts() {
	global $npa_dm_enqueue;
	if ( !$npa_dm_enqueue )
		return;

	wp_print_scripts('npa-dm-scripts');
	wp_print_styles('npa-dm-styles');
}
add_action('init', 'npa_dm_register_scripts');
add_action('wp_footer', 'npa_dm_print_scripts');


// Move SEO below custom meta box stuff
//add_filter('wpseo_metabox_prio', 'move_yoast_metabox_to_bottom');
//function move_yoast_metabox_to_bottom() { return 'low'; }

function mime_type_icon($filename) {
	$info = new SplFileInfo($filename);
	$ext = $info->getExtension();
	$icon_url = plugin_dir_url( __FILE__ ) . 'images/mime/' . $ext . '.png';
	$icon_path = plugin_dir_path( __FILE__ ) . 'images/mime/' . $ext . '.png';
	if (file_exists($icon_path))
		return "<img src='$icon_url' />";
	else
		return '';
}

function get_file_size($filename) {
	$bytes = filesize($filename);
	return size_format($bytes);
}
