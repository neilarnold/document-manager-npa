﻿
$j = jQuery.noConflict();

$j(document).ready(
  function () {
  	
  	$j("#document-search").keyup(function () {
  		var val = $j(this).val().toLowerCase();
  		$j("table.document-manager-table tbody tr").each(function () {
  			var text = $j(this).find("td.search-this").text().toLowerCase();
  			if (text.indexOf(val) != -1) {
  				$j(this).show();
  			} else {
  				$j(this).hide();
  			}
  		});

  		$j("table.document-manager-table").each(function () {
  			//var numOfVisibleRows = $j(this).find('tbody tr:visible').length;
  			var numOfVisibleRows = $j(this).find('tbody tr').filter(function () {
  				return $j(this).css('display') !== 'none';
  			}).length;
  			console.log(numOfVisibleRows);
  			if (numOfVisibleRows > 0) {
  				dmListOpen($j(this));
  			} else {
  				dmListClose($j(this));
  			}
  			
  		});
  	});
  	$j("#document-search").keyup();

  	$j(".dm-section-title").each(function (index) {
  		$j(this).addClass("open");	// default state;
  	});

  	$j(".dm-section-title").click(function() {
  		if ($j(this).hasClass("open")) {
  			$j(this).removeClass("open");
  			$j(this).addClass("closed");
  			$j(this).next(".dm-table-container").slideUp();

  		} else {
  			$j(this).removeClass("closed");
  			$j(this).addClass("open");
  			$j(this).next(".dm-table-container").slideDown();
  		}

  	});
  	
  }

	
);



function dmListOpen(objList) {
	console.log("dmListOpen");
	var div = objList.parent().prev(".dm-section-title");
	if (div.hasClass("closed")) {
		div.removeClass("closed");
		div.addClass("open");
		div.next(".dm-table-container").slideDown();
	}
}
function dmListClose(objList) {
	console.log("dmListClose");
	var div = objList.parent().prev(".dm-section-title");
	console.log(div);
	if (div.hasClass("open")) {
		div.removeClass("open");
		div.addClass("closed");
		div.next(".dm-table-container").slideUp();
	}
}